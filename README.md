# Silvy, my personal chatbot
Silvy is a personal chatbot and most modules are specifically written for me.

Her features include:
* manages / calculates TOTP 2-Factor Authentication codes,
* OpenStreetMap change notifications,
* an advent calendar,
* posting stickers (since Matrix.org doesn’t support our stickers) and
* anniversary reminder.

## Run Silvy
First, you will need to create a user on a Matrix server. This can be https://matrix.org or any other. Log into the account the bot is supposed to use and note down the AccessToken.
```
npm install
cp config/matrix.dist.js config/matrix.js
open config/matrix.js # Configure
npm run start
```
