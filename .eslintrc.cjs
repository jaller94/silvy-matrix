module.exports = {
    'env': {
        'commonjs': true,
        'node': true,
        'es2021': true,
    },
    'extends': 'eslint:recommended',
    'parserOptions': {
        'sourceType': 'module',
    },
    'rules': {
        'indent': [
            'error',
            4,
            {
                'SwitchCase': 1,
            },
        ],
        'linebreak-style': [
            'error',
            'unix',
        ],
        'no-unused-vars': [
            'error',
            {
                'varsIgnorePattern': '^_',
            },
        ],
        'quotes': [
            'error',
            'single',
        ],
        'semi': [
            'error',
            'always',
        ],
    },
    overrides: [
        {
            files: '*.spec.js',
            extends: ['plugin:jest/recommended']
        },
    ]
};
