export default {
    baseUrl: 'https://matrix.org',
    myUserId: '',
    myPassword: '',
    testingRoom: '',
    privateRoom: '',
    stickerUrl: 'https://localhost/',
    chrisId: '',
    modules: [
        'dice',
    ],
    sendReadReceipts: true
};
