import {getRandomInt} from '../../src/helper.js';

const getDiceUnicode = function(x) {
    if (x >= 1 && x <= 6) {
        const index = Math.floor(x)-1;
        return '⚀⚁⚂⚃⚄⚅'.charAt(index);
    }
};

export default class DiceModule {
    constructor(client) {
        this.client = client;
    }

    onMessage(event, roomId) {
        const answer = this.message(event['content'].body.toLowerCase());
        if (answer) {
            this.client.sendText(roomId, answer);
        }
    }

    message(msg) {
        const diceMatch = msg.match(/^dice (\d{1,10})$/);
        let diceSides = Number.parseInt((diceMatch || {})[1]);
        if (diceMatch) {
            return this.throwDice(diceSides);
        } else if (msg === 'coin') {
            return this.throwCoin();
        } else if (msg === 'dice') {
            return this.throwDice();
        }
    }

    throwCoin() {
        return Math.random() < 0.5 ? '⍟ heads' : '① tails';
    }

    throwDice(sides = 6) {
        if (sides < 2) return 'Now that would be pointless.. 😐';
        if (sides == 6) return getDiceUnicode(getRandomInt(1, 6));
        return getRandomInt(1, sides).toString();
    }
}
