import { spawn } from 'node:child_process';

export default class DownloadModule {
    constructor(client) {
        this.client = client;
    }

    onMessage(event, roomId) {
        const message = this.message(event['content'].body);
        if (typeof message === 'string' && message.startsWith('gallery-dl ')) {
            this.client.sendText(roomId, 'Processing…');

            const child = spawn('gallery-dl', ['-lh', '/usr']);
            child.stdout.on('data', (data) => {
                this.client.sendText(roomId, `stdout: ${data}`);
            });

            child.stderr.on('data', (data) => {
                this.client.sendText(roomId, `stderr: ${data}`);
            });

            child.on('close', (code) => {
                this.client.sendText(roomId, `child process exited with code ${code}`);
            });
        }
    }
}
