function isValidBBox(bbox) {
    if (Array.isArray(bbox)) return 'BBox to be an array.';
    if (bbox.length !== 4) return 'Bbox need to have four items.';
    return true;
}

export function findBusStops(bbox) {
    if (!isValidBBox(bbox)) throw new Error('Invalid bbox parameter');
    const bbox_str = `${bbox[0]},${bbox[1]},${bbox[2]},${bbox[3]}`;
    return `[out:json][timeout:25];
(
node["public_transport"="platform"](${bbox_str});
node["highway"="bus_stop"](${bbox_str});
way["highway"="bus_stop"](${bbox_str});
);
// print results
out body;
>;
out skel qt;
`;
}

export function findFireHydrants(bbox) {
    if (!isValidBBox(bbox)) throw new Error('Invalid bbox parameter');
    const bbox_str = `${bbox[0]},${bbox[1]},${bbox[2]},${bbox[3]}`;
    return `[out:json][timeout:25];
(
node["fire_hydrant:type"="pillar"](${bbox_str});
);
// print results
out body;
>;
out skel qt;
`;
}
