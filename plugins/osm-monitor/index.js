'use strict';
import fs from 'node:fs';
import util from 'node:util';

import {count} from '../../src/helper.js';
import {askOverpass, OsmBusStats, OsmHydrantStats} from './osm-helper.js';
import OsmQueryBuilder from './osm-query-builder.js';

import matrixConfig from '../../config/matrix.js';
const {chrisId, privateRoom} = matrixConfig;

const translinkBusesBBox = [
    '49.004',
    '-123.424',
    '49.474',
    '-122.302',
];

const vancouverBBox = [
    '49.2023454582315',
    '-123.22574615478514',
    '49.31438004800689',
    '-123.02352905273438',
];

export default class OsmMonitorModule {
    constructor(client, cronEngine) {
        this.client = client;
        this.cronEngine = cronEngine;
        this.lastBusList = null;
        this.lastHydrantList = null;
        this.roomId = null;

        this.load();
        if (this.roomId !== null) {
            this.startCronJob();
        }
    }

    load() {
        fs.readFile('data/osm.json', 'utf8', (err, content) => {
            if (err) {
                this.catchError('Error reading data/osm.json file:')(err);
            }
            if (content) {
                try {
                    const data = JSON.parse(content);
                    this.lastBusList = data.busList || null;
                    this.lastHydrantList = data.hydrantList || null;
                    this.osmRoomId = data.osmRoomId || null;
                } catch(err) {
                    this.catchError('Error parsing data/osm.json file:')(err);
                }
            }
        });
    }

    save() {
        const json = JSON.stringify({
            busList: this.lastBusList,
            hydrantList: this.lastHydrantList,
            osmRoomId: this.osmRoomId,
        }, null, 4);
        fs.writeFile('data/osm.json', json, 'utf8', (err) => {
            if (err) {
                this.catchError('Error writing the OSM Monitor data.')(err);
            }
        });
    }

    startCronJob() {
        const busDifferencesCron = this.busDifferencesCron.bind(this);
        const hydrantDifferencesCron = this.hydrantDifferencesCron.bind(this);
        const saveCron = this.save.bind(this);
        this.cronEngine.addJob('0 14 * * * *', busDifferencesCron);
        this.cronEngine.addJob('1 14 * * * *', hydrantDifferencesCron);
        this.cronEngine.addJob('2 14 * * * *', saveCron);
    }

    onMessage(event, roomId) {
        const msg = event['content'].body.toLowerCase();
        if (msg === 'init osm room') {
            this.initOsmRoom(event['sender'], roomId);
        } else if (msg === 'bus stop info') {
            this.outputBusDifferences(roomId).catch(
                this.catchError('Error while processing "bus stop info"!')
            );
        } else if (msg === 'fire hydrant info') {
            this.outputHydrantDifferences(roomId).catch(
                this.catchError('Error while processing "fire hydrant info"!')
            );
        }
    }

    async busDifferencesCron() {
        try {
            this.lastBusList = await this.outputBusDifferences(this.roomId, this.lastBusList);
        } catch (err) {
            this.catchError('OSM Module error on bus difference cron job!')(err);
        }
    }

    async hydrantDifferencesCron() {
        try {
            this.lastHydrantList = await this.outputHydrantDifferences(this.roomId, this.lastHydrantList);
        } catch (err) {
            this.catchError('OSM Module error on fire hydrant differences cron job!')(err);
        }
    }

    initOsmRoom(senderId, roomId) {
        if (!this.isAdmin(senderId)) return false;

        if (!this.roomId) this.startCronJob();
        this.roomId = roomId;
        this.save();
        return true;
    }

    isAdmin(senderId) {
        return senderId === chrisId;
    }

    printFact(name, value, diff) {
        let result = `${name}: ${value}`;
        if (diff) {
            result += ` (${diff})`;
        }
        return result;
    }

    async outputBusDifferences(roomId, prevLists) {
        const query = OsmQueryBuilder.findBusStops(translinkBusesBBox);
        const data = await askOverpass(query);
        const lists = OsmBusStats(JSON.parse(data));
        const msg = this.printBusDifferences(prevLists, lists);
        if (msg) {
            const headline = '=== 🚌 Greater Vancouver bus stop info ===';
            this.client.sendText(roomId, `${headline}\n${msg}`);
        }
        return lists;
    }

    async outputHydrantDifferences(roomId, prevLists) {
        const query = OsmQueryBuilder.findFireHydrants(vancouverBBox);
        const data = await askOverpass(query);
        const lists = OsmHydrantStats(JSON.parse(data));
        const msg = this.printHydrantDifferences(prevLists, lists);
        if (msg) {
            const headline = '=== 🚒 Vancouver fire hydrant info ===';
            this.client.sendText(roomId, `${headline}\n${msg}`);
        }
        return lists;
    }

    printBusDifferences(prevLists, lists) {
        let diffAllStops, diffCorrectStops, diffIncorrectStops, diffMissingRef;
        if (prevLists) {
            diffAllStops = count(lists.allStops) - count(prevLists.allStops);
            diffCorrectStops = count(lists.correctStops) - count(prevLists.correctStops);
            diffIncorrectStops = count(lists.incorrectStops) - count(prevLists.incorrectStops);
            diffMissingRef = lists.missingRef.length - prevLists.missingRef.length;
        }

        if (!prevLists || diffAllStops || diffCorrectStops || diffIncorrectStops || diffMissingRef) {
            let msg = this.printFact('Number of identifiable bus stops', count(lists.allStops), diffAllStops);
            msg += '\n' + this.printFact('Number of correct bus stops', count(lists.correctStops), diffCorrectStops);
            msg += '\n' + this.printFact('Number of incorrect bus stops', count(lists.incorrectStops), diffIncorrectStops);
            msg += '\n' + this.printFact('Bus stops missing a ref=*', lists.missingRef.length, diffMissingRef);
            return msg;
        }
    }

    printHydrantDifferences(prevLists, lists) {
        let diffAllHydrants, diffCorrectHydrants, diffOtherHydrants, diffMissingRef;
        if (prevLists) {
            diffAllHydrants = count(lists.allHydrants) - count(prevLists.allHydrants);
            diffCorrectHydrants = count(lists.correctHydrants) - count(prevLists.correctHydrants);
            diffOtherHydrants = count(lists.otherHydrants) - count(prevLists.otherHydrants);
            diffMissingRef = lists.missingRef.length - prevLists.missingRef.length;
        }

        if (!prevLists || diffAllHydrants || diffCorrectHydrants || diffOtherHydrants || diffMissingRef) {
            let msg = this.printFact('Number of identifiable fire hydrants', count(lists.allHydrants), diffAllHydrants);
            msg += '\n' + this.printFact('Number of correct fire hydrants', count(lists.correctHydrants), diffCorrectHydrants);
            msg += '\n' + this.printFact('Number of fire hydrants with other refs', count(lists.otherHydrants), diffOtherHydrants);
            msg += '\n' + this.printFact('Fire hydrants missing a ref=*', lists.missingRef.length, diffMissingRef);
            return msg;
        }
    }

    catchError(msg) {
        return (err) => {
            this.sendErrorMessage(msg, err);
        };
    }

    sendErrorMessage(message, error) {
        let msg = `== ${message} ==\n`;
        msg += util.inspect(error);
        console.error(msg);
        this.client.sendText(privateRoom, msg);
    }
}
