import querystring from 'querystring';

function addStop(stops, id, element) {
    if (id === undefined) return;
    if (stops[id] === undefined) {
        stops[id] = [];
    }
    stops[id].push(element);
}

export async function askOverpass(codestring) {
    // Build the post string from an object
    const post_data = querystring.stringify({
        'data' : codestring,
    });

    const response = await fetch('http://overpass-api.de/api/interpreter', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(post_data),
        },
    });
    return response.json();
}

function includesBusStopNumber(str) {
    return /[56]\d{4}/.test(str);
}

function isBusStopNumber(str) {
    return /^[56]\d{4}$/.test(str);
}

function parseBusStopNumber(str) {
    return Number.parseInt(str.match(/\d{5}/)[0]);
}

// function includesVancouverFireHydrantRef(str) {
//     return /[A-Z]\d{5}/.test(str);
// }

function isVancouverFireHydrantRef(str) {
    return /^[A-Z]\d{5}$/.test(str);
}

function parseVancouverFireHydrantRef(str) {
    return str.match(/[A-Z]\d{5}/)[0];
}

export function OsmBusStats(data) {
    const response = {
        allStops: {},
        correctStops: {},
        incorrectStops: {},
        missingRef: [],
    };

    for (const element of data.elements) {
        if (!element.tags) {
            console.warn(`Node ${element.id} does not have tags!`);
        }
        const tags = element.tags || {};
        let id;
        if (isBusStopNumber(tags.ref)) {
            id = parseBusStopNumber(tags.ref);
            addStop(response.correctStops, id, element);
        } else if (includesBusStopNumber(tags.name)) {
            id = parseBusStopNumber(tags.name);
            addStop(response.incorrectStops, id, element);
        } else if (includesBusStopNumber(tags['name:en'])) {
            id = parseBusStopNumber(tags['name:en']);
            addStop(response.incorrectStops, id, element);
        } else if (includesBusStopNumber(tags['name:fr'])) {
            id = parseBusStopNumber(tags['name:fr']);
            addStop(response.incorrectStops, id, element);
        } else {
            response.missingRef.push(element);
        }
        if (id) {
            addStop(response.allStops, id, element);
        }
    }

    return response;
}

export function OsmHydrantStats(data) {
    const response = {
        allHydrants: {},
        correctHydrants: {},
        otherHydrants: {},
        missingRef: [],
    };

    for (const element of data.elements) {
        const tags = element.tags;
        let id;
        if (isVancouverFireHydrantRef(tags.ref)) {
            id = parseVancouverFireHydrantRef(tags.ref);
            addStop(response.correctHydrants, id, element);
        } else if (tags.ref) {
            id = tags.ref;
            addStop(response.otherHydrants, id, element);
        } else {
            response.missingRef.push(element);
        }
        if (id) {
            addStop(response.allHydrants, id, element);
        }
    }

    return response;
}
