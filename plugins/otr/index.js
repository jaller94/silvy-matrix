import fs from 'node:fs';
import speakeasy from 'speakeasy';

// const reservedNames = 'add,delete,list,remove,rename,rn'.split(',');

export default class OTR {
    constructor(client) {
        this.client = client;
        this.conf = null;
        this.load();
    }

    load() {
        this.conf = JSON.parse(fs.readFileSync('config/otp.json', 'utf8'));
    }

    save() {
        const json = JSON.stringify(this.conf, null, 4);
        fs.writeFileSync('config/otp.json', json, 'utf8', (err) => {
            if (err) {
                console.error('Saving the plugin OTR configuration failed!');
                console.error(err);
            }
        });
    }

    onMessage(event, roomId) {
        const answer = this.message(event['sender'], event['content'].body, event.event.event_id);
        if (answer) {
            this.client.sendText(roomId, answer);
        }
    }

    message(user, msg) {
        if (user in this.conf) {
            if (msg.startsWith('totp list')) {
                let summary = '=== Your TOTP passwords ===\n';
                for (const key in this.conf[user]) {
                    summary += '* ' + key + '\n';
                }
                return summary;
            } else if (msg.startsWith('totp rename ')) {
                const parts = msg.split(' ');
                const oldName = parts[2];
                const newName = parts[3];
                if (!oldName || !newName) {
                    return 'ERROR: Specifiy an old and a new name for this key!';
                }
                this.conf[user][newName] = this.conf[user][oldName];
                delete this.conf[user][oldName];
                this.save();
                return 'Renamed your TOTP key ' + oldName + ' to ' + newName + '!';
            } else if (msg.startsWith('totp ')) {
                for (const [key, entry] of Object.entries(this.conf[user])) {
                    if (msg.endsWith(key)) {
                        const seconds = Math.ceil(Date.now()/1000) + 2;
                        const secret = entry['secret'];
                        const encoding = entry['encoding'] || 'base32';
                        const token = speakeasy.totp({
                            secret: secret,
                            encoding: encoding,
                            time: seconds, // specified in seconds
                        });
                        return token;
                    }
                }
                return 'Key not found!';
            }

            const regex_otpuri = /otpauth:\/\/(htop|totp)\/(.+)%3A(.+)\?secret=(.+)&issuer=(\w+)/;

            if (regex_otpuri.test(msg)) {
                const match = msg.match(regex_otpuri);
                if (match) {
                    const entry = {};
                    entry.type = match[1];
                    entry.product = match[2];
                    entry.title = match[3].replace('%40', '@'); //TODO Add functionality of decodeURI
                    entry.secret = match[4];
                    entry.issuer = match[5]; //TODO Add functionality of decodeURI

                    // Update an old entry if it has the same title and issuer.
                    const old_entry = this.searchEntry(user, entry.product, entry.title, entry.issuer);
                    if (old_entry) {
                        old_entry.type = entry.type;
                        old_entry.secret = entry.secret;
                        old_entry.encoding = entry.encoding;
                    } else {
                        // No old entry found create a new one.
                        this.conf[user]['test'] = entry;
                    }

                    this.save();

                    if (old_entry) {
                        return 'Ok, I updated your entry.';
                    } else {
                        return 'Ok, I saved your new entry. Access it with totp test.';
                    }
                }
            }
        }
    }

    searchEntry(user, product, title, issuer) {
        for (const entry of Object.values(this.conf[user])) {
            if (product !== entry.product) continue;
            if (title !== entry.title) continue;
            if (issuer && issuer !== entry.issuer) continue;
            return entry;
        }
    }
}
