import { getRandomFromArray } from '../../src/helper.js';

const messagesOngoingMatch = [
    'Please wait until the ongoing Tic-Tac-Toe match is over.',
    'There is a Tic-Tac-Toe match ongoing. You wouldn\'t want to interrupt it, would you?',
    'We can play another round once the ongoing match is finished.',
    'An ongoing match of Tic-Tac-Toe has to be finished before I can start a new one.',
];

const messagesTie = [
    'The board is full but noone was able to win the match!',
    'All fields have been checked but no party was able to get three checks in a row!',
    'This game of Tic-Tac-Toe ends in a tie!',
    'Better luck next time! You\'ve tied.',
    'Thanks for playing. Maybe next time there will be a winner.',
];

export default class TicTacToeModule {
    constructor(client) {
        this.client = client;
        this.games = {};
    }

    parseChallenge(msg) {
        const regEx = /tic[ -]?tac[ -]?toe/i;
        if (!regEx.test(msg)) return;

        // Was a user challenged?
        const userRegex = /<a href="https:\/\/matrix.to\/#\/(.*)">(.*)<\/a>/;
        const match = msg.match(userRegex);
        if (match) {
            return match[1];
        }

        // Was the room challenged?
        if (msg.includes('@room')) return '@room';
    }

    placeCheck(roomId, playerId, position) {
        position--; // the array is zero-based; the user input 1-based
        const game = this.games[roomId];
        let playingPlayer;
        if (game.challenger === playerId) {
            playingPlayer = 1;
        }
        if (game.challengee === playerId || game.challengee === '@room') {
            playingPlayer = 2;
        }
        if (game.whoseTurn !== playingPlayer && game.whoseTurn !== -1) {
            return 'not their turn';
        }
        if (game.board[position] !== 0) {
            return 'field already taken';
        }
        game.board[position] = playingPlayer;
        // TODO: Check if a player has won

        // Now it's the other player's turn
        game.whoseTurn = playingPlayer === 1 ? 2 : 1;

        if (game.board.filter(v => (v === 0)).length === 0) {
            this.postBoard(roomId);
            this.client.sendText(roomId, getRandomFromArray(messagesTie));
            this.games[roomId] = undefined;
        }
    }

    drawBoard(board) {
        const place = (value, fallback) => {
            switch (value) {
                case 0: return fallback;
                case 1: return '⏺';
                case 2: return '🔀';
                default: return '⚠️';
            }
        };
        let txt = '';
        txt += place(board[0], '1️⃣');
        txt += place(board[1], '2️⃣');
        txt += place(board[2], '3️⃣');
        txt += '\n';
        txt += place(board[3], '4️⃣');
        txt += place(board[4], '5️⃣');
        txt += place(board[5], '6️⃣');
        txt += '\n';
        txt += place(board[6], '7️⃣');
        txt += place(board[7], '8️⃣');
        txt += place(board[8], '9️⃣');
        return txt;
    }

    postBoard(roomId) {
        const game = this.games[roomId];
        if (!game) {
            this.client.sendText(roomId, 'no game');
            return;
        }
        this.client.sendText(roomId, 'will post board now');
        this.client.sendText(roomId, this.drawBoard(game.board));
    }

    async onMessage(event, roomId) {
        if (!event.content['formatted_body']) return;
        let msg = event.content['formatted_body'].toLowerCase();
        msg = msg.replace(/<\/?p>/ig, '');

        const addressSilvyRegEx = /^<a href="https:\/\/matrix.to\/#\/(.*)">(.*)<\/a>:?/;
        const match = msg.match(addressSilvyRegEx);
        if (!match) return;
        if (match[1] !== await this.client.getUserId()) return;
        msg = msg.substr(match[0].length).trim();
        
        const game = this.games[roomId];
        
        const challengee = this.parseChallenge(msg);
        this.client.sendText(roomId, `${challengee} | ${game}`);
        if (challengee) {
            if (game) {
                this.client.sendText(roomId, getRandomFromArray(messagesOngoingMatch));
            } else {
                this.games[roomId] = {
                    whoseTurn: -1,
                    board: [0,0,0, 0,0,0, 0,0,0],
                    challenger: event.sender,
                    challengee,
                };
                this.postBoard(roomId);
            }
            return;
        }

        if (!game) return;

        const selection = Number.parseInt(msg.trim());
        if (selection >= 1 && selection <= 9) {
            const errorMessage = this.placeCheck(roomId, event.sender, selection);
            if (!errorMessage) {
                this.postBoard(roomId);
            } else {
                this.client.sendText(roomId, errorMessage);
            }
        }
    }
}
