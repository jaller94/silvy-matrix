import TicTacToe from './index.js';

describe('parseChallenge', () => {
    const ticTacToe = new TicTacToe();
    test('challenge to person', () => {
        expect(ticTacToe.parseChallenge('I challenge <a href="https://matrix.to/#/@silvy:matrix.org">Silvy</a> to a round of tic tac toe!')).toBe('@silvy:matrix.org');
        expect(ticTacToe.parseChallenge('I challenge <a href="https://matrix.to/#/@silvy:matrix.org">Silvy</a> to tic tac toe!')).toBe('@silvy:matrix.org');
        expect(ticTacToe.parseChallenge('I want to play Tic-Tac-Toe with <a href="https://matrix.to/#/@silvy:matrix.org">Silvy</a>.')).toBe('@silvy:matrix.org');
        expect(ticTacToe.parseChallenge('I want to play TIC TAC TOE tac toe with <a href="https://matrix.to/#/@silvy:matrix.org">Silvy</a>.')).toBe('@silvy:matrix.org');
        expect(ticTacToe.parseChallenge('Let\'s see who is better at TICTACTOE! <a href="https://matrix.to/#/@silvy:matrix.org">Silvy</a> or me!')).toBe('@silvy:matrix.org');
    });
    test('challenge to room', () => {
        expect(ticTacToe.parseChallenge('I challenge @room to a round of tic tac toe!')).toBe('@room');
        expect(ticTacToe.parseChallenge('I challenge @room to tic tac toe!')).toBe('@room');
        expect(ticTacToe.parseChallenge('I want to play Tic-Tac-Toe with @room.')).toBe('@room');
        expect(ticTacToe.parseChallenge('I want to play TIC TAC TOE tac toe with @room.')).toBe('@room');
        expect(ticTacToe.parseChallenge('Let\'s see who is better at TICTACTOE! @room or me!')).toBe('@room');
    });
    test('not a challenge', () => {
        expect(ticTacToe.parseChallenge('I challenge Mike to a duel!')).toBeUndefined();
    });
});
