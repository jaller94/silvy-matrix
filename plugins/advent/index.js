import fs from 'node:fs';
import util from 'node:util';
import moment from 'moment';

import {fileExtensionToMimeType, uploadFile} from '../../src/helper.js';

import matrixConfig from '../../config/matrix.js';
const {privateRoom, testingRoom, chrisId} = matrixConfig;

export default class AdventModule {
    constructor(client, cronEngine) {
        this.client = client;

        const newDayMessage = this.newDayMessage.bind(this);
        for (let i = 1; i <= 25; i++) {
            cronEngine.addJob('0 8 ' + i + ' 12 * 2018', newDayMessage);
        }
    }

    getFilePath(filename) {
        return `data/advent/${filename}`;
    }

    onMessage(event, roomId) {
        const msg = event['content'].body;
        const sender = event['sender'];
        if (sender === chrisId) {
            let match = msg.match(/^advent (\d{1,2})$/i);
            if (match) {
                const day = match[1];
                this.postDoor(roomId, this.openDoor(day));
            }
            match = msg.match(/^advent-test (\d{1,2})$/i);
            if (match) {
                const day = match[1];
                this.client.sendText(roomId, this.isDay(day).toString());
            }
        }
        if (sender === chrisId) {
            let match = msg.match(/^write advent (\d{1,2})\n([\s\S]*)$/i);
            if (match) {
                const day = match[1];
                const content = match[2];
                this.client.sendText(roomId, this.writeDoor(day, content));
            }
            match = msg.match(/^advent-force (\d{1,2})$/i);
            if (match) {
                const day = match[1];
                this.postDoor(roomId, this.openDoor(day, true));
            }
        }
    }

    openDoor(date, ignoreDateCheck = false) {
        if (this.isDay(date) || ignoreDateCheck) {
            try {
                return fs.readFileSync(this.getFilePath(date + '.txt'), 'utf8');
            } catch (err) {
                const msg = '== Advent Calendar failed to open door ' + date + '! ==\n' + util.inspect(err);
                this.client.sendText(privateRoom, msg);
                return 'Sorry, could not open door... there might not be a door for that day..';
            }
        } else {
            return 'You’ve got to wait a little longer to open this door!';
        }
    }

    postDoor(roomId, content) {
        const regExImage = /img:(\S*)/;
        const cleanedContent = content.replace(regExImage, '').trim();
        if (cleanedContent !== '') {
            this.client.sendText(roomId, cleanedContent);
        }
        const filePath = (content.match(regExImage) || {})[1];
        if (filePath) {
            uploadFile(this.client, this.getFilePath(filePath)).then((img) => {
                const info = {
                    mimetype: fileExtensionToMimeType(filePath),
                };
                this.client.sendImageMessage(roomId, img.content_uri, info, '');
            }).catch((err) => {
                this.client.sendText(roomId, 'There should be an image here... but I can’t find it. 🙁');
                const msg = '== Failed to send an image for the advent calendar! ==\n' + util.inspect(err);
                this.client.sendText(privateRoom, msg);
            });
        }
    }

    writeDoor(date, content) {
        if (Number.parseInt(date)) {
            try {
                fs.writeFileSync(this.getFilePath(date + '.txt'), content, 'utf8');
            } catch (err) {
                const msg = '== Advent Calendar failed to write door ' + date + '! ==\n' + util.inspect(err);
                this.client.sendText(privateRoom, msg);
                return 'Sorry, could not write door...';
            }
            return 'Success!';
        } else {
            return 'Not a valid date!';
        }
    }

    isDay(date) {
        if (date < 1 || date > 25) return false;
        const start = moment('2018-12-01T00:00-08:00').date(date);
        return moment().isSameOrAfter(start);
    }

    newDayMessage() {
        this.client.sendText(testingRoom, 'A new door is available! Say "advent <no>"!');
    }
}
