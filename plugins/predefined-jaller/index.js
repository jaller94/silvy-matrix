import moment from 'moment';


import {getRandomFromArray} from '../../src/helper.js';
import matrixConfig from '../../config/matrix.js';
const {testingRoom, chrisId} = matrixConfig;
import {sendImage, sendMarkdownMessage} from '../../src/messages/sendMethods.js';

export default class JallerModule {
    constructor(client) {
        this.client = client;

        //cronEngine.addJob('30 2 * * 6 *', this.happyFriday.bind(this));
    }

    async onMessage(event, roomId) {
        const user = event['sender'];
        const msg = event['content'].body;

        if ((user === chrisId)) {
            if (msg.toLowerCase().startsWith('say ')) {
                const content = event['content'];
                if (content.format === 'org.matrix.custom.html') {
                    const repeat = msg.substr(4);
                    sendMarkdownMessage(this.client, testingRoom, repeat);
                } else {
                    this.client.sendText(testingRoom, msg.substr(4));
                }
            } else if (msg.toLowerCase().startsWith('repeat ')) {
                const content = event['content'];
                if (content.format === 'org.matrix.custom.html') {
                    const repeat = msg.substr(7);
                    sendMarkdownMessage(this.client, roomId, repeat);
                } else {
                    this.client.sendText(roomId, msg.substr(7));
                }
            } else if (msg.toLowerCase().startsWith('ping')) {
                this.client.sendText(roomId, 'pong').catch(console.error);
            }
        }

        if (msg.toLowerCase() === 'send picture') {
            try {
                await sendImage(this.client, roomId, 'plugins/predefined-jaller/image.png');
            } catch (err) {
                await this.client.sendText(roomId, 'Failed to upload picture!\n'+ err);
            }
        }
    }

    happyFriday() {
        const start = moment('2020-05-30T22:00+02:00');
        const now = moment();
        const celebrations = [
            'Wohooo!',
            'Yesssss!',
            'It has been another week!',
            'It’s that time of the week again!',
            'You’re my favourite two humans!',
            'Fantastic!',
            'Guess what?',
            'Herzlichen Glückwunsch!',
            'Félicitations!',
            'Happy to report:',
            'Hope you two have a lovely evening!',
        ];
        const emojis = '🎈,🎀,🥂,🍾,🎁,🎈,🎀,🎊,🎉,💃,🕺'.split(',');
        const celebration = getRandomFromArray(celebrations);
        const emoji = getRandomFromArray(emojis);
        let message = `${celebration} ${emoji} Happy Friday!\n`;
        message += 'Let’s do the math! You have been together for:\n';
        message += now.diff(start, 'weeks') + ' weeks or\n';
        message += now.diff(start, 'days') + ' days or\n';
        message += now.diff(start, 'hours') + ' hours or\n';
        message += now.diff(start, 'minutes') + ' minutes.';
        this.client.sendText(testingRoom, message);
    }
}
