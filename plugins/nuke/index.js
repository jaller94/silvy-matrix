import util from 'node:util';

export default class NukeModule {
    constructor(client) {
        this.client = client;
        this.var = null;
    }

    async onMessage(event, room) {
        const msg = event['content'].body;
        if (msg.toLowerCase() === 'nuke room') {
            const myUserId = await this.client.whoAmI();
            const members = room.currentState.members;
            if (members[event['sender']].powerLevel === 100) {
                this.client.sendText(room.roomId, 'I think you have the neccessary permissions!');
            } else {
                this.client.sendText(room.roomId, 'I don’t think you have the neccessary permissions!');
            }
            if (members[myUserId].powerLevel === 100) {
                this.client.sendText(room.roomId, 'I think I have the neccessary permissions!');
            } else {
                this.client.sendText(room.roomId, 'I don’t think I have the neccessary permissions!');
            }
            this.client.redactEvent(room.roomId, event.event.event_id, (err) => {
                if (err) {
                    let msg = 'An error occourced while redacting a message.\n';
                    msg += JSON.stringify(err);
                    this.client.sendText(room.roomId, msg);
                }
            });
        }
        if (msg.toLowerCase().startsWith('inspect')) {
            this.client.sendText(room.roomId, util.inspect(room.timeline));
        }
    }
}
