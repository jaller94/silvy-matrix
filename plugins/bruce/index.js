import {getRandomInt} from '../../src/helper.js';

export default class BruceModule {
    constructor(client) {
        this.client = client;
    }

    onMessage(event, roomId) {
        const answer = this.message(event['sender'], event['content'].body);
        if (answer) {
            this.client.sendText(roomId, answer);
        }
    }

    honk(min, max) {
        let amount = min;
        if (max) {
            amount = getRandomInt(min, max);
        }
        return 'honk '.repeat(amount);
    }

    message(user, msg) {
        const nameOccurrences = (msg.match(/(bruce|good goose)/gi) || []).length;
        if (nameOccurrences > 5) {
            return 'honk?!';
        } else if (nameOccurrences !== 0) {
            return this.honk(nameOccurrences);
        }
    }
}
