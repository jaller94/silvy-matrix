const getRandomInt = function(min, max) {
    return Math.floor(Math.random() * (max - min) + min + 1);
};


const wins = [0,1,2,5,10,25,50,100,200,400,1000];

const card = function({rank, suit}) {
    //const back = '🂠';
    //const spades = '🂡🂢🂣🂤🂥🂦🂧🂨🂩🂪🂫🂬🂭🂮'.split('');
    //const hearts = '🂱🂲🂳🂴🂵🂶🂷🂸🂹🂺🂻🂼🂽🂾'.split('');
    //const diamonds = '🃁🃂🃃🃄🃅🃆🃇🃈🃉🃊🃋🃌🃍🃎'.split('');
    //const clubs = '🃑🃒🃓🃔🃕🃖🃗🃘🃙🃚🃛🃝🃞'.split('');
    const suits = '♠♥♦♣'.split('');
    const ranks = 'A 2 3 4 5 6 7 8 9 10 J Q K'.split(' ');
    if (rank < 1 || rank > 12) console.debug('NO CARD', rank);
    const suitSign = suits[suit];
    const rankSign = ranks[rank-1];
    return `[${suitSign}${rankSign}]`;
};

class HigherLowerSession {
    constructor() {
        this.cards = [this.drawCard()];
        this.level = 0;
        this.lost = false;
    }

    allCards() {
        return {...this.cards};
    }

    currentCard() {
        const lastIndex = this.cards.length - 1;
        return this.cards[lastIndex];
    }

    currentPrize() {
        return wins[this.level];
    }

    drawCard() {
        return {
            rank: getRandomInt(1, 13),
            suit: getRandomInt(0, 3),
        };
    }

    isWon() {
        return this.level === wins.length - 1;
    }

    currentLevel() {
        return this.level;
    }

    nextPrize() {
        return wins[this.level + 1];
    }

    play(bet) {
        if (this.lost || this.isWon()) throw new Error('Game is over!');
        if (bet !== 'h' && bet !== 'l') throw new Error('Invalid bet!');
        const currentCard = this.currentCard();
        const newCard = this.drawCard();
        const playerWins = (
            (bet === 'h' && (newCard.rank >= currentCard.rank)) ||
            (bet === 'l' && (newCard.rank <= currentCard.rank))
        );
        this.cards.push(newCard);
        if (playerWins) {
            this.level++;
        } else {
            this.lost = false;
        }
        return playerWins;
    }
}

export default class HigherLowerGameModule {
    constructor(client) {
        this.client = client;
        //this.conf = null;
        this.sessions = {};
        //this.load();
    }
    /*
    load() {
        this.conf = JSON.parse(fs.readFileSync('config/htgame.json', 'utf8'));
    }

    save() {
        const json = JSON.stringify(this.conf, null, 4);
        fs.writeFileSync('config/dice.json', json, 'utf8', (err) => {
            if (err) {
                console.error('Saving the plugin OTR configuration failed!');
                console.error(err);
            }
        });
    }*/

    onMessage(event, roomId) {
        const msg = event['content'].body.toLowerCase();
        const answer = this.message(event['sender'], msg);
        if (answer) {
            this.client.sendText(roomId, answer);
        }
    }

    message(userId, msg) {
        if (msg === 'hl game') {
            return this.startGame(userId);
        } else if (msg === 'h' || msg === 'higher') {
            return this.playLevel(userId, 'h');
        } else if (msg === 'l' || msg === 'lower') {
            return this.playLevel(userId, 'l');
        } else if (msg === 'end') {
            if (!(userId in this.sessions)) return;
            const session = this.sessions[userId];
            this.endGame(userId);
            if (session.currentLevel() === 0) return '🐔';
            const prize = session.currentPrize();
            return `Alright! Here are your ${prize}⛀!`;
        }
    }

    endGame(userId) {
        delete this.sessions[userId];
    }

    playLevel(userId, bet) {
        if (!(userId in this.sessions)) return;
        const session = this.sessions[userId];
        const oldCard = session.currentCard();
        const wonLevel = session.play(bet);
        if (wonLevel) {
            if (session.isWon(session)) {
                const prize = session.currentPrize();
                return card(oldCard) + ' ' + card(session.currentCard()) + '\n' +
                `YAY!!! You win the jackpot of ${prize}⛀!`;
            }
            return card(oldCard) + ' ' + card(session.currentCard()) + '\n'
                + this.printSession(session);
        } else {
            this.endGame(userId);
            return card(oldCard) + ' ' + card(session.currentCard()) + '\n' +
            'Wrong! Game over!';
        }
    }

    printSession(session) {
        return 'Next prize: ' + session.nextPrize() + '⛀\n' +
            'Current card: ' + card(session.currentCard()) + '\n' +
            'H(igher)/L(ower)/End';
    }

    startGame(userId) {
        if (userId in this.sessions) {
            const session = this.sessions[userId];
            return 'You are already in a game!\n' + this.printSession(session);
        } else {
            const session = new HigherLowerSession();
            this.sessions[userId] = session;
            return 'HL Game started!\n' + this.printSession(session);
        }
    }
}
