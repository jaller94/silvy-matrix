import matrixConfig from '../../config/matrix.js';
const {stickerUrl} = matrixConfig;

let stickerPacks = {
    'Eevee': [
        {
            texts: ['🙂', '😊', '☺️', 'neutral',],
            url: stickerUrl + 'eevee01/neutral.png',
        },
        {
            texts: ['😃', '😀', '😄', '😁', 'happy', 'joy',],
            url: stickerUrl + 'eevee01/happy.png',
        },
        {
            texts: ['😍', '😘', 'love',],
            url: stickerUrl + 'eevee01/love.png',
        },
        {
            texts: ['🙃', 'upside-down', 'sarcasm',],
            url: stickerUrl + 'eevee01/upside-down.png',
        },
        {
            texts: ['😦', 'sad',],
            url: stickerUrl + 'eevee01/sad.png',
        },
        {
            texts: ['😳', 'sorry', '🇨🇦', 'canada', 'canadian',],
            url: stickerUrl + 'eevee01/sorry.png',
        },
        {
            texts: ['😢', '😭', 'crying', 'sobbing', 'tears',],
            url: stickerUrl + 'eevee01/crying.png',
        },
        {
            texts: ['⁉️', 'dafuq', 'wtf', '!?',],
            url: stickerUrl + 'eevee01/dafuq.png',
        },
        {
            texts: ['😠', '😡', '💢', 'angry',],
            url: stickerUrl + 'eevee01/angry.png',
        },
        {
            texts: ['😒', 'unamused', 'meh',],
            url: stickerUrl + 'eevee01/unamused.png',
        },
    ],
    'Fox': [
        {
            texts: ['sleep alone',],
            url: stickerUrl + 'fox01/sleep_alone.png',
        },
    ],
    'Pusheen': [
        {
            texts: ['academic', 'expert',],
            url: stickerUrl + 'pusheen01/academic.png',
        },
        {
            texts: ['amused',],
            url: stickerUrl + 'pusheen01/amused.png',
        },
        {
            texts: ['sleeping',],
            url: stickerUrl + 'pusheen01/sleeping.png',
        },
        {
            texts: ['yarn', 'angry yarn',],
            url: stickerUrl + 'pusheen01/angry-yarn.png',
        },
        {
            texts: ['commuting', 'roller',],
            url: stickerUrl + 'pusheen01/commuting.png',
        },
        {
            texts: ['flop', 'no perspective',],
            url: stickerUrl + 'pusheen01/flop.png',
        },
        {
            texts: ['on back', 'not moving',],
            url: stickerUrl + 'pusheen01/on-back.png',
        },
        {
            texts: ['unhappy',],
            url: stickerUrl + 'pusheen01/unhappy.png',
        },
        {
            texts: ['computer',],
            url: stickerUrl + 'pusheen01/computer.png',
        },
        {
            texts: ['long hair', 'fluffy', 'hairy',],
            url: stickerUrl + 'pusheen01/fluffy.png',
        },
        {
            texts: ['really long hair', 'very long hair'],
            url: stickerUrl + 'pusheen01/rapunzel.png',
        },
    ],
    'Unicorn': [
        {
            texts: ['crown me',],
            url: stickerUrl + 'unicorn/crown-me.png',
        },
        {
            texts: ['fame',],
            url: stickerUrl + 'unicorn/fame.png',
        },
        {
            texts: ['hi',],
            url: stickerUrl + 'unicorn/hi.png',
        },
        {
            texts: ['lit',],
            url: stickerUrl + 'unicorn/lit.png',
        },
        {
            texts: ['love you', 'love'],
            url: stickerUrl + 'unicorn/love-you.png',
        },
        {
            texts: ['meh',],
            url: stickerUrl + 'unicorn/meh.png',
        },
        {
            texts: ['morning',],
            url: stickerUrl + 'unicorn/morning.png',
        },
        {
            texts: ['sweet dreams', 'night', 'good night', 'gn8'],
            url: stickerUrl + 'unicorn/sweet-dreams.png',
        },
    ],
};

export default class StickersModule {
    constructor(client) {
        this.client = client;
    }

    onMessage(event, roomId) {
        if (event['content']['msgtype'] !== 'm.text') return;

        const msg = event['content'].body.toLowerCase();

        if (/^do it!?$/.test(msg)) {
            this.client.sendMessage(roomId, {
                body: '[Horse with a fake horn]\nDon’t dream it, do it!',
                msgtype: 'm.image',
                url: stickerUrl + 'special/do-it.jpg',
            });
        }

        for (const stickerPackName in stickerPacks) {
            const stickerPackKeyword = stickerPackName.toLowerCase();
            if (msg.startsWith(stickerPackKeyword + ' ')) {
                const statement = msg.substr(stickerPackKeyword.length + 1);
                if (statement === 'list') {
                    let summary = '=== ' + stickerPackName + ' stickers ===';
                    for (const sticker of stickerPacks[stickerPackName]) {
                        summary += '\n' + sticker.texts.join(', ');
                    }
                    this.client.sendText(roomId, summary);
                    break;
                }
                const sticker = stickerPacks[stickerPackName].find((sticker) => {
                    return sticker.texts.includes(statement);
                });
                if (sticker) {
                    this.client.sendMessage(roomId, {
                        body: sticker.url,
                        msgtype: 'm.image',
                        url: sticker.url,
                    });
                    break;
                }
            }
        }
    }
}
