'use strict';

import fs from 'node:fs';
import util from 'node:util';

import matrixConfig from '../../config/matrix.js';
const {privateRoom, testingRoom, chrisId} = matrixConfig;

export  default class PocketModule {
    constructor(client) {
        this.client = client;
        this.owes = [];
        this.load();
    }

    load() {
        this.owes = JSON.parse(fs.readFileSync('config/pocket.json', 'utf8'));
    }

    save() {
        const json = JSON.stringify(this.owes, null, 4);
        fs.writeFileSync('config/pocket.json', json, 'utf8', (err) => {
            if (err) {
                console.error('Saving the pocket storage failed!');
                console.error(err);
                this.client.sendText(testingRoom, 'Pocket: Unable to save pocket change!');
                this.client.sendText(privateRoom, 'Saving the pocket storage failed!');
                this.client.sendText(privateRoom, util.inspect(err));
            }
        });
    }

    onMessage(event, roomId) {
        const lines = event['content'].body.split('\n');
        for (const line of lines) {
            const answer = this.owesMe(line, event['sender'], event.event.event_id);
            if (answer) {
                this.client.sendText(roomId, answer);
                this.save();
            }
        }
    }

    owesMe(msg, poster, eventId) {
        if (msg.toLowerCase() === 'pocket') {
            return this.pocket();
        } else if (msg.toLowerCase() === 'pocket merge') {
            return this.pocket(true);
        } else if (msg.toLowerCase() === 'pocket raw') {
            return this.pocketRaw();
        }

        const match = msg.match(/(\w+) owes (\w+) ([$ \d,.-]+)(?:\((.*)\))?/);
        if (match) {
            const debtor = this.whoIs(match[1], poster);
            const debtee = this.whoIs(match[2], poster);
            const amount = this.parseMoney(match[3]);
            const category = match[4];

            if (!debtor) return 'Pocket: I’m not sure who the debtor is..';
            if (!debtee) return 'Pocket: I’m not sure who the debtee is..';
            if (!amount || amount.isNaN()) return 'Pocket: I’m not sure how much the amount is..';

            // Remember this.
            const event = {
                eventId,
                message: msg,
                debtor,
                debtee,
                amount,
                category,
                created: new Date(),
            };

            this.owes.push(event);

            const categoryString = (category ? ' (' + category + ')' : '');
            return 'Pocket: ' + debtor + ' owes ' + debtee + ' $' + amount.toFixed(2) + categoryString + '.';
        }
    }

    owesMeRedact(eventId) {
        const deletes = this.owes.filter((owe) => {
            return eventId === owe.eventId;
        });
        let summary = '';
        for (const owe of deletes) {
            summary = deletes.join(', ');
            const index = this.owes.indexOf(owe);
            this.owes = this.owes.splice(index, 1);
            summary += '\nPocket: REDACTION of ' + owe.debtor + ' owes ' + owe.debtee + ' $' + owe.amount.toFixed(2);
        }
        return summary;
    }

    /**
     * Creates a summary of current owings.
     * @param {boolean} merge Should the name "Shared" be split between all other balances?
     * @returns {string}
     */
    pocket(merge = false) {
        const balances = {};
        for (const owe of this.owes) {
            if (balances[owe.debtor] === undefined) balances[owe.debtor] = 0;
            if (balances[owe.debtee] === undefined) balances[owe.debtee] = 0;

            balances[owe.debtor] -= owe.amount;
            balances[owe.debtee] += owe.amount;
        }

        if (merge) {
            if (balances['Shared']) {
                balances['Chris'] += (balances['Shared'] / 2);
                balances['Other'] += (balances['Shared'] / 2);
                delete balances['Shared'];
            }
        }

        let summary = '=== Balances ===\n';
        for (const [person, balance] of Object.entries(balances)) {
            summary += '* ' + person + ': $' + balance.toFixed(2) + '\n';
        }

        return summary;
    }

    /**
     * Parses a string describing an amount of money.
     * @param {string} str
     * @returns {number} A float or NaN
     */
    parseMoney(str) {
        str = str.replace(/,/, '').replace(/[$ ]/, '');
        const float = Number.parseFloat(str);
        if (Number.isNaN(float.isNaN())) return NaN;
        return Number.parseFloat(float.toFixed(2));
    }

    /**
     * @returns {string}
     */
    pocketRaw() {
        return util.inspect(this.owes);
    }

    whoIs(name, poster) {
        name = name.toLowerCase();
        if (name === 'i' || name === 'me') {
            if (poster === chrisId) {
                return 'Chris';
            }
        }
        if (name === 'chris' || name === 'christian' || name === 'jaller') {
            return 'Chris';
        } else if (name === 'shared') {
            return 'Shared';
        }
    }

    handleTimeLine(event, roomId) {
        if (event['content']['msgtype'] === 'm.room.redaction') {
            const answer = this.owesMeRedact(event.event.redacts);
            if (answer) {
                this.client.sendText(roomId, answer);
            }
        }
    }
}
