import { jest } from '@jest/globals';
import CronJob from './CronJob.js';

test('Detects Saturday.', () => {
    const fn = jest.fn();
    const job = new CronJob('0 0 * * 6 *', fn);
    expect(job.test(new Date(Date.UTC(2000, 0, 1, 0, 0)))).toBe(true);
    expect(job.test(new Date(Date.UTC(2000, 0, 2, 0, 0)))).toBe(false);
    expect(job.test(new Date(Date.UTC(2000, 0, 3, 0, 0)))).toBe(false);
    expect(job.test(new Date(Date.UTC(2000, 0, 4, 0, 0)))).toBe(false);
    expect(job.test(new Date(Date.UTC(2000, 0, 5, 0, 0)))).toBe(false);
    expect(job.test(new Date(Date.UTC(2000, 0, 6, 0, 0)))).toBe(false);
    expect(job.test(new Date(Date.UTC(2000, 0, 7, 0, 0)))).toBe(false);
});

test('Detects time 1:00.', () => {
    const fn = jest.fn();
    const job = new CronJob('0 1 * * * *', fn);
    expect(job.test(new Date(Date.UTC(2000, 0, 1, 0, 0)))).toBe(false);
    expect(job.test(new Date(Date.UTC(2000, 0, 1, 0, 59)))).toBe(false);
    expect(job.test(new Date(Date.UTC(2000, 0, 1, 1, 0)))).toBe(true);
    expect(job.test(new Date(Date.UTC(2000, 0, 1, 1, 1)))).toBe(false);
    expect(job.test(new Date(Date.UTC(2000, 0, 1, 2, 0)))).toBe(false);
    expect(job.test(new Date(Date.UTC(2000, 0, 1, 13, 0)))).toBe(false);
});

test('Calls the function on run()', () => {
    const fn = jest.fn();
    const job = new CronJob('0 1 * * * *', fn);
    expect(fn).toHaveBeenCalledTimes(0);
    job.run();
    expect(fn).toHaveBeenCalledTimes(1);
});
