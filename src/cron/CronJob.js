// const dows = 'sun,mon,tue,wed,thu,fri,sat,sun';

export default class CronJob {
    constructor(line, command) {
        //const regex = /^(\d+|\*) (\d+|\*) (.+|\*) (.+|\*) (\d|\*) ([\d\,\-\*\/]+) (.*?)(#.*)?$/;
        const regex = /^(\d+|\*) (\d+|\*) (.+|\*) (.+|\*) (\d|\*) ([\d,\-*/]+)/;
        const match = line.match(regex);
        if (!match) return;
        this.minute = match[1];
        this.hour = match[2];
        this.dom = match[3];
        this.month = match[4];
        this.dow = match[5];
        this.year = match[6];
        this.command = command;
    }

    test(date) {
        if (!(this.minute === '*' ||
                parseInt(this.minute) === date.getUTCMinutes()))
            return false;
        if (!(this.hour === '*' ||
                parseInt(this.hour) === date.getUTCHours()))
            return false;
        if (!(this.dom === '*' ||
                parseInt(this.dom) === date.getUTCDate()))
            return false;
        if (!(this.month === '*' ||
                parseInt(this.month) === date.getUTCMonth()+1))
            return false;
        if (!(this.dow === '*' ||
                parseInt(this.dow) === date.getUTCDay()))
            return false;
        return true;
    }

    async run() {
        await this.command();
    }
}
