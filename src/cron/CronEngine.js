'use strict';
import util from 'node:util';
import CronJob from './CronJob.js';

import matrixConfig from '../../config/matrix.js';
const {privateRoom} = matrixConfig;

export default class CronEngine {
    constructor(client) {
        this.client = client;
        this.jobs = [];
        this.timeout = null;
    }

    addJob(line, command) {
        const job = new CronJob(line, command);
        if (job) {
            this.jobs.push(job);
        }
    }

    start() {
        if (!this.timeout) {
            this._tick();
        }
    }

    stop() {
        !!this.timeout && clearTimeout(this.timeout);
    }

    _tick() {
        const now = new Date();
        this._setAccurateTimeout(1);
        for (const job of this.jobs) {
            if (job.test(now)) {
                const promise = job.run();
                promise.catch((err) => {
                    let msg = '== Promise Error on executing a cron job! ==\n';
                    msg += util.inspect(err);
                    console.error(msg);
                    this.client.sendText(privateRoom, msg);
                });
            }
        }
    }

    _setAccurateTimeout(minutes) {
        const now = new Date();
        const y = now.getUTCFullYear();
        const m = now.getUTCMonth();
        const d = now.getUTCDate();
        const h = now.getUTCHours();
        const nextMinute = new Date(Date.UTC(y, m, d, h, now.getUTCMinutes() + minutes));
        const milliSecsUntil = nextMinute.getTime() - now.getTime();
        //console.debug(now.getTime() / 1000, nextMinute.getTime() / 1000);
        //console.debug('milliSecsUntil', milliSecsUntil)
        this.timeout = setTimeout(this._tick.bind(this), milliSecsUntil);
    }
}
