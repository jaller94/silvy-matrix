'use strict';
import marked from 'marked';

import { uploadFile } from '../helper.js';

export const sendImage = async (client, roomId, filePath, text) => {
    const contentUri = await uploadFile(client, filePath);
    return await client.sendMessage(roomId, {
        body: text || '',
        msgtype: 'm.image',
        url: contentUri,
    });
};

export const sendMarkdownMessage = (client, roomId, markdown) => {
    return client.sendHtmlMessage(roomId, markdown, marked(markdown));
};
