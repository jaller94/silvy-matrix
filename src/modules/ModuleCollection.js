export default class ModuleCollection {
    constructor() {
        this.modules = [];
    }

    add(module) {
        this.modules.push(module);
    }

    async propagateEvent(event, roomId) {
        const errors = [];
        for (const loadedModule of this.modules) {
            try {
                await loadedModule.onMessage(event, roomId);
            } catch (err) {
                errors.push(err);
            }
        }
        return {
            errors,
        };
    }
}
