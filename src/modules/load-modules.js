import path from 'node:path';

const loadModule = async (moduleName) => {
    const modulePath = path.resolve('plugins', moduleName, 'index.js');
    return (await import(modulePath)).default;
};

const loadModules = async (modulesNames) => {
    const promises = [];
    for (const moduleName of modulesNames) {
        const promise = loadModule(moduleName);
        promise.catch((err) => {
            console.error('Error while loading module: ' + moduleName);
            console.error(err);
            throw Error(`Error while loading module: ${moduleName}`);
        });
        promises.push(promise);
    }
    return Promise.all(promises);
};

export default loadModules;
