import { jest } from '@jest/globals';
import ModuleCollection from './ModuleCollection.js';

test('when no modules are loaded doesn’t fail on propagateEvent', async() => {
    const collection = new ModuleCollection();
    const errors = await collection.propagateEvent({}, {}, {});
    expect(errors).toEqual({ errors: [] });
});

test('Forwards calls to propagateEvents', () => {
    const module = {
        onMessage: jest.fn(),
    };
    const event = {};
    const roomId = 'test';
    const collection = new ModuleCollection();
    collection.add(module);
    expect(module.onMessage).not.toHaveBeenCalled();
    collection.propagateEvent(event, roomId);
    expect(module.onMessage).toHaveBeenCalledTimes(1);
    expect(module.onMessage).toHaveBeenCalledWith(event, roomId);
});
