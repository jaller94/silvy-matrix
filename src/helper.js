'use strict';
import fsPromises from 'node:fs/promises';
import path from 'node:path';

export const count = (array) => {
    let count = 0;
    for(const _ in array) {
        count++;
    }
    return count;
};

const mimeTypesByExtension = {
    bmp: 'image/bmp',
    css: 'text/css',
    html: 'text/html',
    gif: 'image/gif',
    jpeg: 'image/jpeg',
    jpg: 'image/jpeg',
    js: 'text/javascript',
    midi: 'audio/midi',
    mp4: 'video/mp4',
    //extend with https://wiki.xiph.org/MIME_Types_and_File_Extensions
    ogg: 'audio/ogg',
    pdf: 'application/pdf',
    png: 'image/png',
    svg: 'image/svg+xml',
    txt: 'text/plain',
    webm: 'video/webm',
    xml: 'application/xml',
};

/**
 * @param {string} filename
 * @returns {string=} mimetype
 */
export const fileExtensionToMimeType = (filename) => {
    if (typeof filename !== 'string') return;
    const extension = filename.split('.').pop();
    return mimeTypesByExtension[extension];
};

/**
 * @param {unknown[]} array
 * @param {unknown}
 */
export const getRandomFromArray = (array) => {
    return array[Math.floor(Math.random()*array.length)];
};

/**
 * @param {number} min
 * @param {number} max
 * @returns {number}
 */
export const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min);
};

/**
 * @param {unknown} variable
 * @param {string[]} path
 * @returns {unknown=}
 */
export const getVariable = (variable, path) => {
    if (!path) return variable;
    if (typeof variable !== 'undefined' || typeof variable[path[0]] !== 'undefined') {
        return getVariable(variable[path[0]], path.slice(1));
    }
};

/**
 * @param {MatrixClient} client
 * @param {string} filename
 */
export const uploadFile = async (client, filename) => {
    const data = await fsPromises.readFile(filename);

    return await client.uploadContent(
        data,
        fileExtensionToMimeType(filename),
        path.basename(filename),
    );
};
