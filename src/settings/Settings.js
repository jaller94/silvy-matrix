import fsPromises from 'node:fs/promises';

export default class Settings {
    constructor() {
        this.settings = {
            'global': {},
        };
    }

    _getRoomSettings(room = 'global') {
        return this.settings[room];
    }

    _getOrCreateRoomSettings(room = 'global') {
        this.settings[room] = this.settings[room] || {};
        return this.settings[room];
    }

    _getOrCreateUserSettings(roomSettings, user) {
        roomSettings.users = roomSettings.users || {};
        const users = roomSettings.users;
        users[user] = users[user] || {};
        return users[user];
    }

    _getUserSettings(settings, user) {
        if (settings && settings['users']) {
            return settings['users'][user];
        }
    }

    _getOrCreateModuleSettings(settings, module) {
        settings['active_modules'] = settings['active_modules'] || {};
        const activeModules = settings['active_modules'];
        activeModules[module] = activeModules[module] || {};
        return activeModules[module];
    }

    _getModuleSettings(settings, module) {
        if (settings && settings['active_modules']) {
            return settings['active_modules'][module];
        }
    }

    _searchModuleSettings(module, {room, user}) {
        const roomSettings = this._getOrCreateRoomSettings(room);
        const settings = (!user
            ? roomSettings
            : this._getOrCreateUserSettings(roomSettings, user)
        );
        return this._getOrCreateModuleSettings(settings, module);
    }

    isModuleActive(module, {room, user}) {
        let output;
        output = (this._getModuleSettings(this._getRoomSettings(), module) || {}).enabled;
        if (output === false) return false;
        if (user) {
            const newoutput = (this._getModuleSettings(this._getUserSettings(this._getRoomSettings(), user), module) || {}).enabled;
            if (newoutput === false) return false;
            output = output || newoutput;
        }
        if (room) {
            const newoutput = (this._getModuleSettings(this._getRoomSettings(room), module) || {}).enabled;
            if (newoutput === false) return false;
            output = output || newoutput;
        }
        if (room && user) {
            const newoutput = (this._getModuleSettings(this._getUserSettings(this._getRoomSettings(room), user), module) || {}).enabled;
            if (newoutput === false) return false;
            output = output || newoutput;
        }
        return output;
    }

    toJSON() {
        return JSON.stringify(this.settings, undefined, 2);
    }

    setModuleStatus(module, value, room = 'global', user) {
        const roomSettings = this._getOrCreateRoomSettings(room);
        const settings = (!user
            ? roomSettings
            : this._getOrCreateUserSettings(roomSettings, user)
        );
        const moduleSettings = this._getOrCreateModuleSettings(settings, module);
        moduleSettings.enabled = value;
    }

    static async fromFile(filePath) {
        const data = await fsPromises.readFile(filePath, 'utf8');
        return JSON.parse(data);
    }

    async toFile(filePath) {
        await fsPromises.writeFile(filePath, 'utf8', this.toJSON());
    }
}
