import {fileExtensionToMimeType, getRandomInt} from './helper.js';

test('Detects MIME type from a file name', () => {
    expect(fileExtensionToMimeType('test.jpg')).toBe('image/jpeg');
    expect(fileExtensionToMimeType('test.gif')).toBe('image/gif');
    expect(fileExtensionToMimeType('test.png')).toBe('image/png');
});

test('Does not detect a MIME type with a white space at the end', () => {
    expect(fileExtensionToMimeType('test.jpg ')).toBeUndefined();
    expect(fileExtensionToMimeType('test.gif ')).toBeUndefined();
    expect(fileExtensionToMimeType('test.png ')).toBeUndefined();
});

test('getRandomInt(0, x) is not higher than x', () => {
    for (let x = 0; x < 1000; x++) {
        expect(getRandomInt(0, x)).toBeLessThanOrEqual(x);
    }
});

test('getRandomInt(x, x * 2) is not lower than x', () => {
    for (let x = 0; x < 1000; x++) {
        expect(getRandomInt(x, x * 2)).toBeGreaterThanOrEqual(x);
    }
});
