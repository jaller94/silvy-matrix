export const logMessageEvent = (event, roomId) => {
    console.info(`(${roomId}) ${event['sender']} :: ${event['content'].body}`);
};
